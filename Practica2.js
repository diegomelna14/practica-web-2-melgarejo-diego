function guardar() {
    var nombre = document.getElementById('nombre').value;
    var apellidos = document.getElementById('apellidos').value;
    var curso = document.getElementById('curso').value;

    if (nombre == '' || apellidos == '' || curso == '') {
        alert("Rellene todos los campos")
    }
    else {
        var table = document.getElementById('tabla');
        var row = table.insertRow(-1);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        cell1.innerHTML = nombre;
        cell2.innerHTML = apellidos;
        cell3.innerHTML = curso;
    }
}

function cancelar() {
    document.getElementById('nombre').value = '';
    document.getElementById('apellidos').value = '';
    document.getElementById('curso').value = '';
}